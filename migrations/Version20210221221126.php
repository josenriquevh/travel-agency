<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210221221126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE travel (id INT AUTO_INCREMENT NOT NULL, codigo INT NOT NULL, plazas INT NOT NULL, destino VARCHAR(100) NOT NULL, origen VARCHAR(100) NOT NULL, precio DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajero (id INT AUTO_INCREMENT NOT NULL, cedula INT NOT NULL, nombre VARCHAR(100) NOT NULL, fecha_nacimiento DATE NOT NULL, telefono VARCHAR(15) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajero_travel (id INT AUTO_INCREMENT NOT NULL, viajero_id INT NOT NULL, travel_id INT NOT NULL, fecha_viaje DATE NOT NULL, fecha_retorno DATE DEFAULT NULL, INDEX IDX_E2F87A1AFAF4CFE9 (viajero_id), INDEX IDX_E2F87A1AECAB15B3 (travel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE viajero_travel ADD CONSTRAINT FK_E2F87A1AFAF4CFE9 FOREIGN KEY (viajero_id) REFERENCES viajero (id)');
        $this->addSql('ALTER TABLE viajero_travel ADD CONSTRAINT FK_E2F87A1AECAB15B3 FOREIGN KEY (travel_id) REFERENCES travel (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE viajero_travel DROP FOREIGN KEY FK_E2F87A1AECAB15B3');
        $this->addSql('ALTER TABLE viajero_travel DROP FOREIGN KEY FK_E2F87A1AFAF4CFE9');
        $this->addSql('DROP TABLE travel');
        $this->addSql('DROP TABLE viajero');
        $this->addSql('DROP TABLE viajero_travel');
    }
}
