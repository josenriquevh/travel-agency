$(document).ready(function() {

    $(".masked").inputmask();

    $('#form').validate({
        rules: {
            cedula: {
                required: true,
                digits: true
            },
            nombre: {
                required: true
            },
            fechaNacimiento: {
                required: true
            },
            telefono: {
                required: true
            }
        },
        messages: {
            cedula: {
                required: "La cédula es requerida",
                digits: "La cédula debe ser numérica"
            },
            nombre: {
                required: "El nombre es requerido"
            },
            fechaNacimiento: {
                required: "La fecha de nacimiento es requerida"
            },
            telefono: {
                required: "El teléfono es requerido"
            }
        }
    });

    $('#guardar').click(function(){
        var valid = $("#form").valid();
        if (valid){
            $('#form').submit();
        }
    });
    
});

