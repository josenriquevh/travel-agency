$(document).ready(function() {

    $(".masked").inputmask();

    $('#form').validate({
        rules: {
            fechaViaje: {
                required: true
            }
        },
        messages: {
            fechaViaje: {
                required: "La fecha del viaje es requerida"
            }
        }
    });

    $('.travel').click(function(){
        $('#travelId-tfoot').hide();
    });

    $('#guardar').click(function(){
        
        var valid = $("#form").valid();
        if (valid){

            // Chequear si se seleccionó un viaje
            var checked = false;
            $('.travel').each(function(){
                if ($(this).is(':checked')){
                    checked = true;
                }
            });

            if (checked === true){
                $('#form').submit();
            }
            else {
                $('#travelId-error').html('Debe seleccionar un viaje');
                $('#travelId-tfoot, #travelId-error').show();
            }

        }

    });
    
});

