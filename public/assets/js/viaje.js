$(document).ready(function() {

    $('#form').validate({
        rules: {
            codigo: {
                required: true,
                digits: true
            },
            plazas: {
                required: true,
                digits: true,
                min: 1
            },
            precio: {
                required: true,
                min: 0,
                number: true
            },
            origen: {
                required: true
            },
            destino: {
                required: true
            }
        },
        messages: {
            codigo: {
                required: "El código es requerido",
                digits: "El código debe ser numérico"
            },
            plazas: {
                required: "La cantidad de plazas es requerida",
                digits: "La cantidad de plazas debe ser numérica",
                min: "La cantidad de plazas ser mayor o igual a 1",
            },
            precio: {
                required: "El precio es requerido",
                min: "El precio debe ser mayor a cero",
                number: "El precio debe ser numérico"
            },
            origen: {
                required: "El origen es requerido"
            },
            destino: {
                required: "El destino es requerido"
            }
        }
    });

    $('#guardar').click(function(){
        var valid = $("#form").valid();
        if (valid){
            $('#form').submit();
        }
    });
    
});

