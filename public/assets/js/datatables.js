// Call the dataTables jQuery plugin
$(document).ready(function() {
	//$('#dataTable').DataTable();
	var table = $('#dataTable').DataTable( {
	    language: {
	    	processing: "Procesando...",
	        searchPlaceholder: "Buscar registros",
	        sSearch: '',
	        sLengthMenu: "Mostrar _MENU_",
	        sLength: 'dataTables_length',
	        sZeroRecords: "No se encontraron resultados",
	        sEmptyTable:  "Ningún dato disponible en esta tabla",
	        sInfo: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_.",
	        sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
	        sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
	        oPaginate: {
	            sPrevious: "Previo",
	            sNext: "Siguiente"
	        }
	    }
	} );
});
