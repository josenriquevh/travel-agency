# Travel Agency

Desarrollo de un API que permite la gestión básica de una agencia de viajes, tales como llevar el control de los clientes que acuden a la agencia y los viajes que éstos realizan.

## Descripción Breve del Proyecto

### [Proyecto desarrollado en Symfony 5.2.3](https://symfony.com/doc/current/index.html)

- [Installing & Setting up the Symfony Framework](https://symfony.com/doc/current/setup.html)
- [How to Deploy a Symfony Application](https://symfony.com/doc/current/deployment.html)
- [Symfony Flex](https://symfony.com/doc/current/setup/flex.html)
- [Creating the Project (Best Practices)](https://symfony.com/doc/current/best_practices.html)
- [Databases and the Doctrine ORM](https://symfony.com/doc/current/doctrine.html)
- [Routing](https://symfony.com/doc/current/routing.html)
- [HTTP Client](https://symfony.com/doc/current/http_client.html)

## Configuración del ambiente local

### Instalación

1. Clonar el proyecto  git clone https://gitlab.com/josenriquevh/travel-agency.git`.
2. Ejecutar `composer install`.
3. Crear virtualhost. **Opcionalmente se puede ejecutar `php bin/console server:run` para levantar un servidor**.


        <VirtualHost *:80>
            ServerName     api-viajes.net
            ServerAlias    apiviajes.net

            DocumentRoot "${INSTALL_DIR}/www/travel-agency/public"
            DirectoryIndex  index.php
            <Directory "${INSTALL_DIR}/www/travel-agency/public">
                AllowOverride None
                Allow from All
                <IfModule mod_rewrite.c>
                    Options -MultiViews
                    RewriteEngine On
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteRule ^(.*)$ index.php [QSA,L]
                </IfModule>
            </Directory>

            SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
            SetEnv APP_ENV dev
        </VirtualHost>


4. Los parámetros de la BD se crean en el archivo _.env_. Aunque se puede crear el archivo _.env.local_ para colocar los parámetros propios de su ambiente local:
    `DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7`
5. Si es primera vez que se configura la BD, ejecutar el comando `php bin/console doctrine:database:create`. Esto creará la BD.
6. Ejecutar el siguiente comando para actualizar los atributos de las tablas de la BD: `php bin/console doctrine:migrations:migrate`.
7. En la ruta _${INSTALL_DIR}/www/travel-agency/_
    - Crear las carpetas cache, log y sessions (si no existen) con `mkdir -p var/cache var/log var/sessions`.
    - Configurar permisos a las carpetas cache, log y sessions con `chmod -R 777 var/cache/ var/log/ var/sessions/`.
    - Borrar el contenido de las carpetas cache y log con `sudo rm -Rf var/cache/**/* var/log/* var/sessions/**/*`.
8. Ejecutar los comandos para que se pueda eliminar la cache usando el comando de symfony. [Documentación](https://symfony.com/doc/current/cache.html#clearing-the-cache).

9. Si todo esta bien, ejecute `api-viajes.net`


## Diagrama de Entidad y Relación

![Diagrama de las entidades utilizada para el API y sus relaciones](https://gitlab.com/josenriquevh/travel-agency/-/raw/master/public/assets/img/diagrama_entidad.png)


## API Reference

### CRUD Viajeros

1. Listar Viajeros
	- Petición GET {dominio}/api/v1/viajeros
	- Parámetros: Ninguno
	- Respuesta: Objetos de la clase Viajero
2. Obtener Viajero
	- Petición GET {dominio}/api/v1/viajeros/{id}
	- Parámetros:
		- id: in="path", type="integer", required="si", description="Identificador del Viajero"
	- Respuesta: Objeto de la clase Viajero
3. Agregar Viajero
	- Petición POST {dominio}/api/v1/viajeros
	- Parámetros:
		- cedula: in="body", type="integer", required="si", description="Cédula del Viajero"
		- nombre: in="body", type="string", required="si", description="Nombre del Viajero"
		- fechaNacimiento: in="body", type="string", required="si", description="Fecha de nacimiento del Viajero en formato AAAA-MM-DD"
		- telefono: in="body", type="string", required="si", description="Teléfono del Viajero"
	- Respuesta: Objeto creado de la clase Viajero
4. Actualizar Viajero
	- Petición PUT {dominio}/api/v1/viajeros/{id}
	- Parámetros:
		- cedula: in="body", type="integer", required="no", description="Cédula del Viajero"
		- nombre: in="body", type="string", required="no", description="Nombre del Viajero"
		- fechaNacimiento: in="body", type="string", required="no", description="Fecha de nacimiento del Viajero en formato AAAA-MM-DD"
		- telefono: in="body", type="string", required="no", description="Teléfono del Viajero"
	- Respuesta: Objeto actualizado de la clase Viajero
5. Eliminar Viajero
	- Petición DELETE {dominio}/api/v1/viajeros/{id}
	- Parámetros:
		- id: in="path", type="integer", required="si", description="Identificador del Viajero"
	- Respuesta: Mensaje existoso


### CRUD Viajes

1. Listar Viajeros
	- Petición GET {dominio}/api/v1/viajes
	- Parámetros: Ninguno
	- Respuesta: Objetos de la clase Travel
2. Obtener Viajero
	- Petición GET {dominio}/api/v1/viajes/{id}
	- Parámetros:
		- id: in="path", type="integer", required="si", description="Identificador del Viaje"
	- Respuesta: Objeto de la clase Travel
3. Agregar Viaje
	- Petición POST {dominio}/api/v1/viajes
	- Parámetros:
		- codigo: in="body", type="integer", required="si", description="Código del Viaje"
		- plazas: in="body", type="integer", required="si", description="Cantidad de plazas del Viaje"
		- destino: in="body", type="string", required="si", description="Destino del Viaje"
		- origen: in="body", type="string", required="si", description="Origen del Viaje"
		- precio: in="body", type="float", required="si", description="Precio del Viaje"
	- Respuesta: Objeto creado de la clase Travel
4. Actualizar Viaje
	- Petición PUT {dominio}/api/v1/viajes/{id}
	- Parámetros:
		- codigo: in="body", type="integer", required="no", description="Código del Viaje"
		- plazas: in="body", type="integer", required="no", description="Cantidad de plazas del Viaje"
		- destino: in="body", type="string", required="no", description="Destino del Viaje"
		- origen: in="body", type="string", required="no", description="Origen del Viaje"
		- precio: in="body", type="float", required="no", description="Precio del Viaje"
	- Respuesta: Objeto actualizado de la clase Travel
5. Eliminar Viaje
	- Petición DELETE {dominio}/api/v1/viajes/{id}
	- Parámetros:
		- id: in="path", type="integer", required="si", description="Identificador del Viaje"
	- Respuesta: Mensaje existoso


### Asignación de Viajes

1. Viajes asignados a un Viajero
	- Petición GET {dominio}/api/v1/viajesViajero/{viajeroId}
	- Parámetros:
		- viajeroId: in="path", type="integer", required="si", description="Identificador del Viajero"
	- Respuesta: Objetos de la clase ViajeroTravel
2. Asignar Viaje
	- Petición POST {dominio}/api/v1/viajesViajero
	- Parámetros:
		- viajeroId: in="body", type="integer", required="si", description="Identificador del Viajero"
		- travelId: in="body", type="integer", required="si", description="Identificador del Viaje"
		- fechaViaje: in="body", type="string", required="si", description="Fecha del viaje en formato AAAA-MM-DD"
		- fechaRetorno: in="body", type="string", required="no", description="Fecha de retorno del viaje en formato AAAA-MM-DD"
	- Respuesta: Objeto creado de la clase ViajeroTravel
