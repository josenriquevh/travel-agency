<?php

namespace App\Controller;

use App\Controller\BaseController as BaseAPIController;
use App\Entity\Viajero;
//use FOS\RestBundle\Controller\Annotations as Rest;
//use Nelmio\ApiDocBundle\Annotation\Model;
//use Swagger\Annotations as SWG;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Viajero Controller
 *
 * @Route("/api/v1/viajeros")
 */
class ViajeroController extends BaseAPIController
{

    /**
     * @Route("", name="viajeros_list", methods={"GET"})
     */
    public function getAllAction()
    {

        try {
            $records = $this->em->getRepository("App:Viajero")->findViajeros();
        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($records);
    }

    /**
     * @Route("/{id}", name="viajeros_get", methods={"GET"})
     */
    public function getViajeroAction($id)
    {

        try {

            $record = $this->em->getRepository("App:Viajero")->find($id);
            if (!$record || is_null($record)) {
                return $this->JsonResponseNotFound();
            }

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($record);
    }

    /**
     * @Route("", name="viajeros_add", methods={"POST"})
     */
    public function addViajeroAction(Request $request)
    {

        try {
            $cedula = $request->request->get("cedula", null);
            $nombre = $request->request->get("nombre", null);
            $fechaNacimiento = $request->request->get("fechaNacimiento", null);
            $telefono = $request->request->get("telefono", null);

            $errores = array();
            $error = 0;

            if (is_null($cedula)) {
                $error = 1;
                $errores[] = array('value' => $cedula,
                                   'info'  => "La cédula es requerida");
            }
            else {
                $cedulaExistente = $this->em->getRepository("App:Viajero")->findCedulaExistente($cedula);
                if ($cedulaExistente){
                    $error = 1;
                    $errores[] = array('value' => $cedula,
                                       'info'  => "Cédula existente");
                }
            }

            if (is_null($nombre)) {
                $error = 1;
                $errores[] = array('value' => $nombre,
                                   'info'  => "El nombre es requerido");
            }

            if (is_null($fechaNacimiento)) {
                $error = 1;
                $errores[] = array('value' => $fechaNacimiento,
                                   'info'  => "La fecha de nacimiento es requerida");
            } 
            else {
                $d = \DateTime::createFromFormat("Y-m-d", $fechaNacimiento);
                if (!($d && $d->format("Y-m-d") === $fechaNacimiento)) {
                    $error = 1;
                    $errores[] = array('value' => $fechaNacimiento,
                                       'info'  => "La fecha de nacimiento debe estar en formato AAAA-MM-DD");
                } 
                else {
                    $fechaNacimiento = new \DateTime($fechaNacimiento);
                    if ($fechaNacimiento->format('Y-m-d') >= date('Y-m-d')) {
                        $error = 1;
                        $errores[] = array('value' => $fechaNacimiento->format('Y-m-d'),
                                           'info'  => "La fecha de nacimiento debe ser menor al día de hoy");
                    }
                }
            }

            if (is_null($telefono)) {
                $error = 1;
                $errores[] = array('value' => $telefono,
                                   'info'  => "El teléfono es requerido");
            }

            if ($error == 1){
                $response['message'] = $errores;
                return $this->JsonResponseBadRequest($response);
            }

            $viajero = new Viajero();
            $viajero->setCedula($cedula);
            $viajero->setNombre($nombre);
            $viajero->setFechaNacimiento($fechaNacimiento);
            $viajero->setTelefono($telefono);

            // Verificar datos de la Entidad
            $errors = $this->validator->validate($viajero);
            if (count($errors) > 0) {
                return $this->JsonResponseError($errors, 'validator');
            }

            $this->em->persist($viajero);
            $this->em->flush();

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($viajero, 201);

    }

    /**
     * @Route("/{id}", name="viajeros_update", methods={"PUT"})
     */
    public function updateViajeroAction(Request $request, $id)
    {

        try {
            $cedula = $request->request->get("cedula", null);
            $nombre = $request->request->get("nombre", null);
            $fechaNacimiento = $request->request->get("fechaNacimiento", null);
            $telefono = $request->request->get("telefono", null);

            $modificar = false;
            $errores = array();
            $error = 0;

            $viajero = $this->em->getRepository("App:Viajero")->find($id);
            if (!$viajero || is_null($viajero)) {
                return $this->JsonResponseNotFound();
            }

            if (trim($cedula) && !is_null($cedula) && $viajero->getCedula() != $cedula) {
                $cedulaExistente = $this->em->getRepository("App:Viajero")->findCedulaExistente($cedula, $id);
                if ($cedulaExistente){
                    $error = 1;
                    $errores[] = array('value' => $cedula,
                                       'info'  => "Cédula existente");
                }
                else {
                    $viajero->setCedula($cedula);
                    $modificar = true;
                }
            }

            if (trim($nombre) && !is_null($nombre) && $viajero->getNombre() != $nombre) {
                $viajero->setNombre($nombre);
                $modificar = true;
            }

            if (trim($fechaNacimiento) && !is_null($fechaNacimiento) && $viajero->getFechaNacimiento()->format('Y-m-d') != $fechaNacimiento) {
                $d = \DateTime::createFromFormat("Y-m-d", $fechaNacimiento);
                if (!($d && $d->format("Y-m-d") === $fechaNacimiento)) {
                    $error = 1;
                    $errores[] = array('value' => $fechaNacimiento,
                                       'info'  => "La fecha de nacimiento debe estar en formato AAAA-MM-DD");
                } 
                else {
                    $fechaNacimiento = new \DateTime($fechaNacimiento);
                    if ($fechaNacimiento->format('Y-m-d') >= date('Y-m-d')) {
                        $error = 1;
                        $errores[] = array('value' => $fechaNacimiento->format('Y-m-d'),
                                           'info'  => "La fecha de nacimiento debe ser menor al día de hoy");
                    }
                    else {
                        $viajero->setFechaNacimiento($fechaNacimiento);
                        $modificar = true;
                    }
                }
            }

            if (trim($telefono) && !is_null($telefono) && $viajero->getTelefono() != $telefono) {
                $viajero->setTelefono($telefono);
                $modificar = true;
            }

            if ($error == 1){
                $response['message'] = $errores;
                return $this->JsonResponseBadRequest($response);
            }

            // Verificar datos de la Entidad
            $errors = $this->validator->validate($viajero);
            if (count($errors) > 0) {
                return $this->JsonResponseError($errors, 'validator');
            }

            if ($modificar) {
                $this->em->persist($viajero);
                $this->em->flush();
            } 
            else {
                return $this->JsonResponseSuccess($viajero, 200, "¡Registro sin alterar!");
            }

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($viajero, 200, "¡Registro modificado con éxito!");

    }

    /**
     * @Route("/{id}", name="viajeros_delete", methods={"DELETE"})
     */
    public function deleteViajeroAction($id)
    {

        try {

            $viajero = $this->em->getRepository("App:Viajero")->find($id);
            if (!$viajero || is_null($viajero)) {
                return $this->JsonResponseNotFound();
            }

            // Eliminar toda la información del viajero
            $vts = $this->em->getRepository("App:ViajeroTravel")->findViajes($id);
            foreach ($vts as $viajeroTravel){
                $this->em->remove($viajeroTravel);
                $this->em->flush();
            }

            $this->em->remove($viajero);
            $this->em->flush();

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess('Viajero ID: '.$id, 200, "¡Registro eliminado con éxito!");
        
    }
    
}
