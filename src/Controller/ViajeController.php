<?php

namespace App\Controller;

use App\Controller\BaseController as BaseAPIController;
use App\Entity\ViajeroTravel;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Viaje Controller
 *
 * @Route("/api/v1/viajesViajero")
 */
class ViajeController extends BaseAPIController
{

    /**
     * @Route("/{viajeroId}", name="viaje_list", methods={"GET"})
     */
    public function getViajesAction($viajeroId)
    {

        try {

            $viajero = $this->em->getRepository("App:Viajero")->find($viajeroId);
            if (!$viajero || is_null($viajero)) {
                return $this->JsonResponseNotFound();
            }

            $records = $this->em->getRepository("App:ViajeroTravel")->findViajes($viajeroId);

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($records);
    }

    /**
     * @Route("", name="viaje_add", methods={"POST"})
     */
    public function addViajeAction(Request $request)
    {

        try {
            $viajeroId = $request->request->get("viajeroId", null);
            $travelId = $request->request->get("travelId", null);
            $fechaViaje = $request->request->get("fechaViaje", null);
            $fechaRetorno = $request->request->get("fechaRetorno", null);

            $errores = array();
            $error = 0;

            if (is_null($viajeroId)) {
                $error = 1;
                $errores[] = array('value' => $viajeroId,
                                   'info'  => "El ID del viajero es requerido");
            }
            else {
                $viajero = $this->em->getRepository("App:Viajero")->find($viajeroId);
                if (!$viajero){
                    $error = 1;
                    $errores[] = array('value' => $viajeroId,
                                       'info'  => "Viajero no existente");
                }
            }

            if (is_null($travelId)) {
                $error = 1;
                $errores[] = array('value' => $travelId,
                                   'info'  => "El ID del viaje es requerido");
            }
            else {
                $travel = $this->em->getRepository("App:Travel")->find($travelId);
                if (!$travel){
                    $error = 1;
                    $errores[] = array('value' => $travelId,
                                       'info'  => "Viaje no existente");
                }
            }

            if (is_null($fechaViaje)) {
                $error = 1;
                $errores[] = array('value' => $fechaViaje,
                                   'info'  => "La fecha del viaje es requerida");
            } 
            else {
                $d = \DateTime::createFromFormat("Y-m-d", $fechaViaje);
                if (!($d && $d->format("Y-m-d") === $fechaViaje)) {
                    $error = 1;
                    $errores[] = array('value' => $fechaViaje,
                                       'info'  => "La fecha del viaje debe estar en formato AAAA-MM-DD");
                } 
                else {
                    $fechaViaje = new \DateTime($fechaViaje);
                    if ($fechaViaje->format('Y-m-d') <= date('Y-m-d')) {
                        $error = 1;
                        $errores[] = array('value' => $fechaViaje->format('Y-m-d'),
                                           'info'  => "La fecha del viaje debe ser mayor al día de hoy");
                    }
                }
            }

            if (!is_null($fechaRetorno)) {
                $d = \DateTime::createFromFormat("Y-m-d", $fechaRetorno);
                if (!($d && $d->format("Y-m-d") === $fechaRetorno)) {
                    $error = 1;
                    $errores[] = array('value' => $fechaRetorno,
                                       'info'  => "La fecha de retorno debe estar en formato AAAA-MM-DD");
                } 
                else {
                    $fechaRetorno = new \DateTime($fechaRetorno);
                    if ($fechaRetorno->format('Y-m-d') <= $fechaViaje->format('Y-m-d')) {
                        $error = 1;
                        $errores[] = array('value' => $fechaRetorno->format('Y-m-d'),
                                           'info'  => "La fecha de retorno debe ser mayor a la fecha del viaje");
                    }
                }
            }

            if ($error == 1){
                $response['message'] = $errores;
                return $this->JsonResponseBadRequest($response);
            }

            $viajeroTravel = new ViajeroTravel();
            $viajeroTravel->setViajero($viajero);
            $viajeroTravel->setTravel($travel);
            $viajeroTravel->setFechaViaje($fechaViaje);
            if (!is_null($fechaRetorno)){
                $viajeroTravel->setFechaRetorno($fechaRetorno);
            }
            $this->em->persist($viajeroTravel);
            $this->em->flush();

            // Se resta una plaza del viaje
            $plazas = $travel->getPlazas() - 1;
            $travel->setPlazas($plazas);
            $this->em->persist($travel);
            $this->em->flush();

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($viajeroTravel, 201);

    }
    
}
