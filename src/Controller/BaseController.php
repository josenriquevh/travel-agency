<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * Esta clase funciona como la Base de todos los Controllers del API
 * 
 * BaseController sirve para controllar las respuesta de los endpoints
 * Response con code: 200, 201, 204, 400, 401, 403, 404, 500
 * 
 * Métodos para pre-formatear las consultas a las base de datos
 * 
 * Referencias:
 * https://blog.restcase.com/5-basic-rest-api-design-guidelines/
 * 
 * @author José Velásquez.
 */
class BaseController
{
    protected $em;
    protected $validator;
    protected $serializer;

    public function __construct(ValidatorInterface $validator, EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->validator = $validator;
        $this->em = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * Método genérico de response (SOLO PARA PRUEBAS)
     */
    public function JsonResponse($data)
    {
        $response = [];
        $response['data'] = $data;
        $response['error'] = false;

        return new JsonResponse($this->serializer->serialize($response, 'json'), 200, [], true);
    }

    /**
     * Método genérico que controla y retornar peticiones exitosas del API
     */
    public function JsonResponseSuccess($data, $code = 200, $msg = NULL)
    {
        $response = [];
        $response['code'] = $code;
        $response['status'] = $code;
        $response['data'] = $data;
        $response['message'] = $msg ?? "¡Consulta Exitosa!";
        $response['error'] = false;

        if ($code == 200 && gettype($data) === 'array') {
            $response['message'] = count($data) > 0 ? "¡Registro(s) encontrado(s)!" : "¡Ningún registro encontrado!";
            $response['totalRecords'] = count($data);
        } 
        elseif ($code == 200 && (is_null($data) || empty($data))) {
            $response['message'] = "¡Sin contenido!";
        } 
        elseif ($code == 200 && is_null($msg) && gettype($data) === 'object') {
            $response['message'] = "¡Registro encontrado!";
        }

        if ($code == 201) {
            $response['message'] = "¡Registro exitoso!";
        }

        return new JsonResponse($this->serializer->serialize($response, 'json'), $code, [], true);
    }

    /**
     * Método genérico que controla y retornar los erroes de Peticiones Malas
     */
    public function JsonResponseBadRequest($data)
    {
        $response = [];
        $response['code'] = 400;
        $response['status'] = 400;
        $response['message'] = "¡Error, por favor ingrese parámetro(s) faltante(s)!";
        $response['error'] = true;

        if (key_exists('info', $data)) {
            $response['info'] = $data['info'];
        }

        if (key_exists('value', $data)) {
            $response['value'] = $data['value'];
        }

        if (key_exists('message', $data)) {
            $response['message'] = $data['message'];
        } 
        elseif (key_exists('info', $data)) {
            $response['message'] = $data['info'];
        }

        return new JsonResponse($this->serializer->serialize($response, 'json'), 400, [], true);
    }

    /**
     * Método genérico que controla y retorna los errores de Acceso Denegado
     */
    public function JsonResponseAccessDenied($message = null)
    {
        $response = [];
        $response['code'] = 403;
        $response['status'] = 403;
        $response['message'] = $message ?? "¡Lo siento; usted no tiene permiso para acceder a este recurso!";
        $response['error'] = true;

        return new JsonResponse($this->serializer->serialize($response, 'json'), 403, [], true);
    }

    /**
     * Método genérico para registro no encontrado del API
     */
    public function JsonResponseNotFound($msg = NULL)
    {
        $response = [];
        $response['code'] = 404;
        $response['status'] = 404;
        $response['message'] = $msg ?? "¡Registro no encontrado!";
        $response['error'] = false;

        return new JsonResponse($this->serializer->serialize($response, 'json'), 404, [], true);
    }

    /**
     * Método genérico que controla y retorna los diferentes errores del API
     */
    public function JsonResponseError($data, $option)
    {
        $response = [];
        $response['code'] = 500;
        $response['status'] = 500;
        $response['error'] = true;

        if ($option == 'exception') {
            $response['message'] = "Ocurrió un error en el Servidor";
            $response['exception'] = $data->getMessage();
            $response['type'] = 'exception';
        }

        if ($option == 'validator') {
            $response['message'] = $this->formatValidationErrors($data);
            $response['description'] = "Error en uno de los campos del registro";
            $response['code'] = 400;
            $response['status'] = 400;
            $response['type'] = 'validator';
        }

        return new JsonResponse($this->serializer->serialize($response, 'json'), $response['code'], [], true);
    }

    /**
     * Formatea el objeto proporcionado por el validator
     *
     * @param  object $data
     * @return array $errors
     */
    private function formatValidationErrors($data)
    {
        $errors = [];

        foreach ($data as $value) {
            $errors[] = [
                "value" => "Campo: ".$value->getPropertyPath(),
                "info" => $value->getMessage(),
            ];
        }

        return $errors;
    }

    /**
     * Convierte un valor en float
     */
    public function convertToFloat($number)
    {
        return floatval(sprintf("%.2f", number_format($number, 2, '.', '')));
    }

}
