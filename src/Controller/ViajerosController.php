<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * @Route("/viajeros")
 */
class ViajerosController extends AbstractController
{

    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/list", name="front_viajeros_index", methods={"GET"})
     */
    public function index()
    {

        $viajeros = array();

        $response = $this->client->request(
            'GET',
            $this->getParameter('urlAPI') . "/api/v1/viajeros"
        );

        if ($response->getStatusCode() == 200){
            $content = $response->toArray();
            $viajeros = $content["data"];
        }

        return $this->render('viajeros/index.html.twig', array('viajeros' => $viajeros));

    }

    /**
     * @Route("/edit/{id}", name="front_viajeros_edit")
     */
    public function edit(Request $request, $id = 0)
    {

        $viajero = array();

        if ($request->getMethod() == 'POST'){
            $fechaNacimiento = $request->request->get('fechaNacimiento');
            list($d,$m,$a) = explode("/", $fechaNacimiento);
            $fechaNacimiento = "$a-$m-$d";
            $viajero = array('id'                        => $id,
                             'cedula'                    => $request->request->get('cedula'),
                             'nombre'                    => $request->request->get('nombre'),
                             'fechaNacimiento'           => $fechaNacimiento,
                             'fechaNacimientoFormateado' => $request->request->get('fechaNacimiento'),
                             'telefono'                  => $request->request->get('telefono'));
            if ($id){
                $method = 'PUT';
                $url = $this->getParameter('urlAPI') . "/api/v1/viajeros/".$id;
            }
            else {
                $method = 'POST';
                $url = $this->getParameter('urlAPI') . "/api/v1/viajeros";
            }
            $response = $this->client->request(
                $method,
                $url, [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'body' => $viajero
                ]          
            );

            if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201){
                $content = $response->toArray();
                $this->addFlash('success', $content["message"]);
                return $this->redirectToRoute('front_viajeros_index');
            }
            else {
                $content = json_decode($response->getContent(false));
                foreach ($content->message as $message){
                    $this->addFlash('error', $message->value.': '.$message->info);
                }
            }
            
        }
        else {
            if ($id){
                $response = $this->client->request(
                    'GET',
                    $this->getParameter('urlAPI') . "/api/v1/viajeros/".$id
                );

                if ($response->getStatusCode() == 200){
                    $content = $response->toArray();
                    $viajero = $content["data"];
                    $fecha = new \DateTime($viajero["fechaNacimiento"]);
                    $viajero["fechaNacimiento"] = $fecha->format('Y-m-d');
                    $viajero["fechaNacimientoFormateado"] = $fecha->format('d/m/Y');
                }
            }
            else {
                $viajero = array('id'                        => $id,
                                 'cedula'                    => '',
                                 'nombre'                    => '',
                                 'fechaNacimiento'           => '',
                                 'fechaNacimientoFormateado' => '',
                                 'telefono'                  => '');
            }
        }

        return $this->render('viajeros/edit.html.twig', array('viajero' => $viajero));

    }

    /**
     * @Route("/asignar/{id}", name="front_viajeros_asignar")
     */
    public function asignar(Request $request, $id = 0)
    {

        $viajero = array();
        $travels = array();
        $fechaViajeFormateado = "";
        $fechaRetornoFormateado = "";

        // Datos del Viajero
        $response = $this->client->request(
            'GET',
            $this->getParameter('urlAPI') . "/api/v1/viajeros/".$id
        );
        if ($response->getStatusCode() == 200){
            $content = $response->toArray();
            $viajero = $content["data"];
        }

        // Lista de Viajes
        $response = $this->client->request(
            'GET',
            $this->getParameter('urlAPI') . "/api/v1/viajes"
        );
        if ($response->getStatusCode() == 200){
            $content = $response->toArray();
            $travels = $content["data"];
        }

        if ($request->getMethod() == 'POST'){

            $fechaViajeFormateado = $request->request->get('fechaViaje');
            list($d,$m,$a) = explode("/", $fechaViajeFormateado);
            $fechaViaje = "$a-$m-$d";

            $viaje = array('viajeroId'  => $id,
                           'travelId'   => $request->request->get('travelId'),
                           'fechaViaje' => $fechaViaje);

            $fechaRetornoFormateado = $request->request->get('fechaRetorno');
            if ($fechaRetornoFormateado != ''){
                list($d,$m,$a) = explode("/", $fechaRetornoFormateado);
                $fechaRetorno = "$a-$m-$d";
                $viaje["fechaRetorno"] = $fechaRetorno;
            }

            
            $response = $this->client->request(
                'POST',
                $this->getParameter('urlAPI') . "/api/v1/viajesViajero", [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'body' => $viaje
                ]          
            );
            
            if ($response->getStatusCode() == 201){
                $content = $response->toArray();
                $this->addFlash('success', $content["message"]);
                return $this->redirectToRoute('front_viajeros_index');
            }
            else {
                $content = json_decode($response->getContent(false));
                foreach ($content->message as $message){
                    $this->addFlash('error', $message->value.': '.$message->info);
                }
            }
            
        }

        return $this->render('viajeros/asignar.html.twig', array('viajero'      => $viajero,
                                                                 'travels'      => $travels,
                                                                 'fechaViaje'   => $fechaViajeFormateado,
                                                                 'fechaRetorno' => $fechaRetornoFormateado));

    }

    /**
     * @Route("/delete", name="front_viajeros_delete", methods={"POST"})
     */
    public function ajaxDelete(Request $request)
    {

        $id = $request->request->get('id');

        $response = $this->client->request(
            'DELETE',
            $this->getParameter('urlAPI') . "/api/v1/viajeros/".$id
        );
        
        if ($response->getStatusCode() == 200){
            $content = $response->toArray();
            $msg = $content["message"];
            $this->addFlash('success', $content["message"]);
        }
        else {
            $content = json_decode($response->getContent(false));
            $msg = $content->message;
            $this->addFlash('error', $content->message);
        }

        $return = array('ok'  => 1,
                        'msg' => $msg);

        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));

    }

}
