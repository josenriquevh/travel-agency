<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * @Route("/viajes")
 */
class ViajesController extends AbstractController
{

    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/list", name="front_viajes_index", methods={"GET"})
     */
    public function index()
    {

        $viajes = array();

        $response = $this->client->request(
            'GET',
            $this->getParameter('urlAPI') . "/api/v1/viajes"
        );

        if ($response->getStatusCode() == 200){
            $content = $response->toArray();
            $viajes = $content["data"];
        }

        return $this->render('viajes/index.html.twig', array('viajes' => $viajes));

    }

    /**
     * @Route("/edit/{id}", name="front_viajes_edit")
     */
    public function edit(Request $request, $id = 0)
    {

        $viaje = array();

        if ($request->getMethod() == 'POST'){
            $viaje = array('id'      => $id,
                           'codigo'  => $request->request->get('codigo'),
                           'plazas'  => $request->request->get('plazas'),
                           'origen'  => $request->request->get('origen'),
                           'destino' => $request->request->get('destino'),
                           'precio'  => $request->request->get('precio'));
            if ($id){
                $method = 'PUT';
                $url = $this->getParameter('urlAPI') . "/api/v1/viajes/".$id;
            }
            else {
                $method = 'POST';
                $url = $this->getParameter('urlAPI') . "/api/v1/viajes";
            }
            $response = $this->client->request(
                $method,
                $url, [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'body' => $viaje
                ]          
            );

            if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201){
                $content = $response->toArray();
                $this->addFlash('success', $content["message"]);
                return $this->redirectToRoute('front_viajes_index');
            }
            else {
                $content = json_decode($response->getContent(false));
                foreach ($content->message as $message){
                    $this->addFlash('error', $message->value.': '.$message->info);
                }
            }
            
        }
        else {
            if ($id){
                $response = $this->client->request(
                    'GET',
                    $this->getParameter('urlAPI') . "/api/v1/viajes/".$id
                );

                if ($response->getStatusCode() == 200){
                    $content = $response->toArray();
                    $viaje = $content["data"];
                }
            }
            else {
                $viaje = array('id'      => $id,
                               'codigo'  => '',
                               'plazas'  => '',
                               'origen'  => '',
                               'destino' => '',
                               'precio'  => '');
            }
        }

        return $this->render('viajes/edit.html.twig', array('viaje' => $viaje));

    }

    /**
     * @Route("/delete", name="front_viajes_delete", methods={"POST"})
     */
    public function ajaxDelete(Request $request)
    {

        $id = $request->request->get('id');

        $response = $this->client->request(
            'DELETE',
            $this->getParameter('urlAPI') . "/api/v1/viajes/".$id
        );
        
        if ($response->getStatusCode() == 200){
            $content = $response->toArray();
            $msg = $content["message"];
            $this->addFlash('success', $content["message"]);
        }
        else {
            $content = json_decode($response->getContent(false));
            $msg = $content->message;
            $this->addFlash('error', $content->message);
        }

        $return = array('ok'  => 1,
                        'msg' => $msg);

        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));

    }

}
