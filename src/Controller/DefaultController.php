<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    public function index()
    {
    	return $this->render('default/index.html.twig');
    }

    public function formatearFecha($fechaStr){
        
        $fecha = new \DateTime($fechaStr);
        return new Response($fecha->format('d/m/Y'));

    }

    public function calcularEdad($fechaStr){
        
        $edad = "";
        $datetime1 = new \DateTime($fechaStr);
		$datetime2 = new \DateTime("now");
		$interval = $datetime1->diff($datetime2);
		
		if ($interval->format('%y') < 1){
			// Si es menos que un año, se contabiliza los meses
			if ($interval->format('%m') < 1){
				// Si es menos que un mes, se contabilizan los días
				$edad = $interval->format('%d').' días';
			}
			else {
				$edad = $interval->format('%m').' meses';
			}
		}
		else {
			$year = $interval->format('%y')==1 ? 'año' : 'años';
			$edad = $interval->format('%y').' '.$year;
		}
        return new Response($edad);

    }

}
