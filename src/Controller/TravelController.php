<?php

namespace App\Controller;

use App\Controller\BaseController as BaseAPIController;
use App\Entity\Travel;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Travel Controller
 *
 * @Route("/api/v1/viajes")
 */
class TravelController extends BaseAPIController
{

    /**
     * @Route("", name="viajes_list", methods={"GET"})
     */
    public function getAllAction()
    {

        try {
            $records = $this->em->getRepository("App:Travel")->findViajes();
        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($records);
    }

    /**
     * @Route("/{id}", name="viajes_get", methods={"GET"})
     */
    public function getViajeAction($id)
    {

        try {

            $record = $this->em->getRepository("App:Travel")->find($id);
            if (!$record || is_null($record)) {
                return $this->JsonResponseNotFound();
            }

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($record);
    }

    /**
     * @Route("", name="viajes_add", methods={"POST"})
     */
    public function addViajeAction(Request $request)
    {

        try {
            $codigo = $request->request->get("codigo", null);
            $plazas = $request->request->get("plazas", null);
            $destino = $request->request->get("destino", null);
            $origen = $request->request->get("origen", null);
            $precio = $request->request->get("precio", null);

            $errores = array();
            $error = 0;

            if (is_null($codigo)) {
                $error = 1;
                $errores[] = array('value' => $codigo,
                                   'info'  => "El código es requerido");
            }
            else {
                $codigoExistente = $this->em->getRepository("App:Travel")->findCodigoExistente($codigo);
                if ($codigoExistente){
                    $error = 1;
                    $errores[] = array('value' => $codigo,
                                       'info'  => "Código existente");
                }
            }

            if (is_null($plazas)) {
                $error = 1;
                $errores[] = array('value' => $plazas,
                                   'info'  => "El número de plazas es requerido");
            }
            else {
                if (intval($plazas) <= 0){
                    $error = 1;
                    $errores[] = array('value' => $plazas,
                                       'info'  => "El número de plazas debe ser mayor a cero");
                }
            }

            if (is_null($destino)) {
                $error = 1;
                $errores[] = array('value' => $destino,
                                   'info'  => "El destino es requerido");
            } 
            
            if (is_null($origen)) {
                $error = 1;
                $errores[] = array('value' => $origen,
                                   'info'  => "El origen es requerido");
            }

            if (is_null($precio)) {
                $error = 1;
                $errores[] = array('value' => $precio,
                                   'info'  => "El precio es requerido");
            }
            else {
                if (floatval($precio) <= 0){
                    $error = 1;
                    $errores[] = array('value' => $precio,
                                       'info'  => "El precio debe ser mayor a cero");
                }
            }

            if ($error == 1){
                $response['message'] = $errores;
                return $this->JsonResponseBadRequest($response);
            }

            $travel = new Travel();
            $travel->setCodigo($codigo);
            $travel->setPlazas($plazas);
            $travel->setDestino($destino);
            $travel->setOrigen($origen);
            $travel->setPrecio($this->convertToFloat($precio));

            // Verificar datos de la Entidad
            $errors = $this->validator->validate($travel);
            if (count($errors) > 0) {
                return $this->JsonResponseError($errors, 'validator');
            }

            $this->em->persist($travel);
            $this->em->flush();

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($travel, 201);

    }

    /**
     * @Route("/{id}", name="viajes_update", methods={"PUT"})
     */
    public function updateViajeAction(Request $request, $id)
    {

        try {
            $codigo = $request->request->get("codigo", null);
            $plazas = $request->request->get("plazas", null);
            $destino = $request->request->get("destino", null);
            $origen = $request->request->get("origen", null);
            $precio = $request->request->get("precio", null);

            $modificar = false;
            $errores = array();
            $error = 0;

            $travel = $this->em->getRepository("App:Travel")->find($id);
            if (!$travel || is_null($travel)) {
                return $this->JsonResponseNotFound();
            }

            if (trim($codigo) && !is_null($codigo) && $travel->getCodigo() != $codigo) {
                $codigoExistente = $this->em->getRepository("App:Travel")->findCodigoExistente($codigo, $id);
                if ($codigoExistente){
                    $error = 1;
                    $errores[] = array('value' => $codigo,
                                       'info'  => "Código existente");
                }
                else {
                    $travel->setCodigo($codigo);
                    $modificar = true;
                }
            }

            if (!is_null($plazas) && $travel->getPlazas() != $plazas) {
                if (intval($plazas) < 0){
                    $error = 1;
                    $errores[] = array('value' => $plazas,
                                       'info'  => "El número de plazas debe ser mayor o igual a cero");
                }
                else {
                    $travel->setPlazas($plazas);
                    $modificar = true;
                }
            }

            if (trim($destino) && !is_null($destino) && $travel->getDestino() != $destino) {
                $travel->setDestino($destino);
                $modificar = true;
            }

            if (trim($origen) && !is_null($origen) && $travel->getOrigen() != $origen) {
                $travel->setOrigen($origen);
                $modificar = true;
            }

            if (!is_null($precio) && $travel->getPrecio() != $precio) {
                if (floatval($precio) <= 0){
                    $error = 1;
                    $errores[] = array('value' => $precio,
                                       'info'  => "El precio debe ser mayor a cero");
                }
                else {
                    $travel->setPrecio($this->convertToFloat($precio));
                    $modificar = true;
                }
            }

            if ($error == 1){
                $response['message'] = $errores;
                return $this->JsonResponseBadRequest($response);
            }

            // Verificar datos de la Entidad
            $errors = $this->validator->validate($travel);
            if (count($errors) > 0) {
                return $this->JsonResponseError($errors, 'validator');
            }

            if ($modificar) {
                $this->em->persist($travel);
                $this->em->flush();
            } 
            else {
                return $this->JsonResponseSuccess($travel, 200, "¡Registro sin alterar!");
            }

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess($travel, 200, "¡Registro modificado con éxito!");

    }

    /**
     * @Route("/{id}", name="viajes_delete", methods={"DELETE"})
     */
    public function deleteViajeAction($id)
    {

        try {

            $travel = $this->em->getRepository("App:Travel")->find($id);
            if (!$travel || is_null($travel)) {
                return $this->JsonResponseNotFound();
            }

            // Eliminar toda la información del viaje
            $vts = $this->em->getRepository("App:ViajeroTravel")->findViajeros($id);
            foreach ($vts as $viajeroTravel){
                $this->em->remove($viajeroTravel);
                $this->em->flush();
            }

            $this->em->remove($travel);
            $this->em->flush();

        } catch (Exception $ex) {
            return $this->JsonResponseError($ex, 'exception');
        }

        return $this->JsonResponseSuccess('Viaje ID: '.$id, 200, "¡Registro eliminado con éxito!");

    }
    
}
