<?php

namespace App\Entity;

use App\Repository\ViajeroTravelRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajeroTravelRepository::class)
 */
class ViajeroTravel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Viajero::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $viajero;

    /**
     * @ORM\ManyToOne(targetEntity=Travel::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $travel;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaViaje;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaRetorno;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViajero(): ?Viajero
    {
        return $this->viajero;
    }

    public function setViajero(?Viajero $viajero): self
    {
        $this->viajero = $viajero;

        return $this;
    }

    public function getTravel(): ?Travel
    {
        return $this->travel;
    }

    public function setTravel(?Travel $travel): self
    {
        $this->travel = $travel;

        return $this;
    }

    public function getFechaViaje(): ?\DateTimeInterface
    {
        return $this->fechaViaje;
    }

    public function setFechaViaje(\DateTimeInterface $fechaViaje): self
    {
        $this->fechaViaje = $fechaViaje;

        return $this;
    }

    public function getFechaRetorno(): ?\DateTimeInterface
    {
        return $this->fechaRetorno;
    }

    public function setFechaRetorno(?\DateTimeInterface $fechaRetorno): self
    {
        $this->fechaRetorno = $fechaRetorno;

        return $this;
    }
}
