<?php

namespace App\Repository;

use App\Entity\Travel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Travel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Travel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Travel[]    findAll()
 * @method Travel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TravelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Travel::class);
    }

    /**
     * @return Travel[] Returns an array of Travel objects
    */
    public function findViajes()
    {
        return $this->createQueryBuilder('t')
            ->orderBy('t.codigo', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findCodigoExistente($codigo, $id = 0): ?Travel
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.codigo = :codigo')
            ->andWhere('t.id != :id')
            ->setParameters(array('codigo' => $codigo,
                                  'id'     => $id))
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
