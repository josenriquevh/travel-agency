<?php

namespace App\Repository;

use App\Entity\ViajeroTravel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ViajeroTravel|null find($id, $lockMode = null, $lockVersion = null)
 * @method ViajeroTravel|null findOneBy(array $criteria, array $orderBy = null)
 * @method ViajeroTravel[]    findAll()
 * @method ViajeroTravel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeroTravelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ViajeroTravel::class);
    }

    /**
     * @return ViajeroTravel[] Returns an array of ViajeroTravel objects
     */
    public function findViajes($viajeroId)
    {
        return $this->createQueryBuilder('vt')
            ->andWhere('vt.viajero = :viajeroId')
            ->setParameter('viajeroId', $viajeroId)
            ->orderBy('vt.fechaViaje', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ViajeroTravel[] Returns an array of ViajeroTravel objects
     */
    public function findViajeros($travelId)
    {
        return $this->createQueryBuilder('vt')
            ->andWhere('vt.travel = :travelId')
            ->setParameter('travelId', $travelId)
            ->orderBy('vt.fechaViaje', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

}
