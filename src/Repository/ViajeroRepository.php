<?php

namespace App\Repository;

use App\Entity\Viajero;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Viajero|null find($id, $lockMode = null, $lockVersion = null)
 * @method Viajero|null findOneBy(array $criteria, array $orderBy = null)
 * @method Viajero[]    findAll()
 * @method Viajero[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Viajero::class);
    }

    /**
     * @return Viajero[] Returns an array of Viajero objects
    */
    public function findViajeros()
    {
        return $this->createQueryBuilder('v')
            ->orderBy('v.nombre', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findCedulaExistente($cedula, $id = 0): ?Viajero
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.cedula = :cedula')
            ->andWhere('v.id != :id')
            ->setParameters(array('cedula' => $cedula,
                                  'id'     => $id))
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
